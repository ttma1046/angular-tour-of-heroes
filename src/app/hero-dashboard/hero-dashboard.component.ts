import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/finally';

import { Hero } from '../core/entity/hero';
import { HeroService } from '../hero.service';

@Component({
    selector: 'app-hero-dashboard',
    templateUrl: './hero-dashboard.component.html',
    styleUrls: ['./hero-dashboard.component.css']
})
export class HeroDashboardComponent implements OnInit {
    heroes: Observable<Hero[]>;
    isLoading = false;
    selectedHero: Hero;

    constructor(private heroService: HeroService) { }

    ngOnInit() {
        this.getHeroes();
    }

    getHeroes(): void {
        this.isLoading = true;

        this.heroes = this.heroService.getMockHeroes()
                        .finally(() => this.isLoading = false);
            // .subscribe(heroes => this.heroes = heroes.slice(1, 5));

        this.selectedHero = undefined;
    }

    select(hero: Hero) { this.selectedHero = hero; }
}
