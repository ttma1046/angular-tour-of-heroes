import { Component } from '@angular/core';

import { QuestionService } from '../core/services/question.service';

@Component({
  selector: 'app-job-application',
  templateUrl: './job-application.component.html',
  styleUrls: ['./job-application.component.css']
})
export class JobApplicationComponent {

  questions: any[];
 
  constructor(service: QuestionService) {
    this.questions = service.getQuestions();
  }
}
