import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';

import { AppRoutingModule } from './app-routing.module';

import { InMemoryDataService } from './in-memory-data.service';

import { AppComponent } from './app.component';
import { HeroListComponent } from './hero-list/hero-list.component';
import { HeroDashboardComponent } from './hero-dashboard/hero-dashboard.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { HeroSearchComponent } from './hero-search/hero-search.component';
import { HeroFormComponent } from './hero-form/hero-form.component';
import { MessagesComponent } from './messages/messages.component';

import { HeroService } from './hero.service';
import { MessageService } from './message.service';
import { QuestionControlService } from './question-control.service';
import { AdService } from './ad.service';
import { HeroReactiveFormComponent } from './hero-reactive-form/hero-reactive-form.component';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { DynamicFormQuestionComponent } from './dynamic-form-question/dynamic-form-question.component';
import { JobApplicationComponent } from './job-application/job-application.component';

import { ForbiddenValidatorDirective } from './shared/forbidden-name.directive';
import { UnlessDirective } from './unless.directive';
import { FlyingHeroesPipe } from './flying-heroes.pipe';
import { FlyingHeroesImpurePipe } from './flying-heroes.impure.pipe';
import { HeroAsyncMessageComponent } from './hero-async-message/hero-async-message.component';
import { HighlightDirective } from './highlight.directive';
import { AdDirective } from './ad.directive';
import { MissionService } from './mission.service';
import { AdBannerComponent } from './ad-banner.component';
import { HeroJobAdComponent } from './hero-job-ad.component';
import { HeroProfileComponent } from './hero-profile.component';
import { HeroAppComponent } from './hero-app.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroListComponent,
    HeroDetailComponent,
    MessagesComponent,
    HeroDashboardComponent,
    HeroSearchComponent,
    HeroFormComponent,
    HeroReactiveFormComponent,
    DynamicFormComponent,
    DynamicFormQuestionComponent,
    JobApplicationComponent,
    ForbiddenValidatorDirective,
    UnlessDirective,
    FlyingHeroesPipe,
    FlyingHeroesImpurePipe,
    HeroAsyncMessageComponent,
    HighlightDirective,
    AdDirective,
    AdBannerComponent,
    HeroJobAdComponent,
    HeroProfileComponent,
    HeroAppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    AppRoutingModule
  ],
  entryComponents: [HeroJobAdComponent, HeroProfileComponent],
  providers: [HeroService, MessageService, QuestionControlService, AdService, MissionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
