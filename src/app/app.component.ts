import { Component } from '@angular/core';
import { AdService } from './ad.service';
import { AdItem } from './ad-item';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    title = 'Hero List';
    color = 'yellow';

    ads: AdItem[];

    constructor(private adService: AdService) { }

    ngOnInit() {
        this.ads = this.adService.getAds();
    }
}
