export class Hero {
    constructor(
        public id: number,
        public name: string,
        public addresses?: Address[],
        public power?: string,
        public alterEgo?: string,
        public sidekick?: string
        ) {}
}

export class Flyer extends Hero {
  public canFly: boolean;
}

export class Address {
  street = '';
  city   = '';
  state  = '';
  zip    = '';
}
