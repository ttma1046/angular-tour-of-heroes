import { Component } from '@angular/core';

import { Hero } from '../core/entity/hero';

@Component({
    selector: 'app-hero-form',
    templateUrl: './hero-form.component.html',
    styleUrls: ['./hero-form.component.css']
})
export class HeroFormComponent {

    powers = ['Really Smart', 'Super Flexible',
        'Super Hot', 'Weather Changer'];

    model = new Hero(18, 'Dr IQ', [
        { street: '123 Main', city: 'Anywhere', state: 'CA', zip: '94801' },
        { street: '456 Maple', city: 'Somewhere', state: 'VA', zip: '23226' },
    ], this.powers[0], 'Chuck Overstreet');

    submitted = false;

    onSubmit() { this.submitted = true; }

    // TODO: Remove this when we're done
    get diagnostic() { return JSON.stringify(this.model); }

    newHero() {
        this.model = new Hero(42, '', [], '', '');
    }
}
