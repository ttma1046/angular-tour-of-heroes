import { Component, Input, OnChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { forbiddenNameValidator } from '../shared/forbidden-name.directive';
import { HeroService } from '../hero.service';

import { states } from '../mock-heroes';
import { Address, Hero } from '../core/entity/hero';

@Component({
  selector: 'app-hero-reactive-form',
  templateUrl: './hero-reactive-form.component.html',
  styleUrls: ['./hero-reactive-form.component.css']
})
export class HeroReactiveFormComponent implements OnChanges {
  heroForm: FormGroup; // <--- heroForm is of type FormGroup
  states = states;
  @Input() hero: Hero;

  nameChangeLog: string[] = [];

  constructor(private fb: FormBuilder, private heroService: HeroService) { // <--- inject FormBuilder
    this.createForm();
    this.logNameChange();
  }

  ngOnChanges() {
    this.rebuildForm();
  }

  logNameChange() {
    const nameControl = this.heroForm.get('name');
    nameControl.valueChanges.forEach(
      (value: string) => this.nameChangeLog.push(value)
    );
  }

  createForm() {
    this.heroForm = this.fb.group({ // <-- the parent FormGroup
      name: ['',
        [
          Validators.required,
          Validators.minLength(4),
          forbiddenNameValidator(/bob/i)] // <-- Here's how you pass in the custom validator.
      ],
      // address: this.fb.group(new Address()),
      secretLairs: this.fb.array([]),
      power: ['', Validators.required],
      sidekick: '',
      alterEgo: ''
    });
  }

  setForm() {
    this.heroForm.setValue({
      name: this.hero.name,
      address: this.hero.addresses[0] || new Address(),
      power: this.hero.power,
      sidekick: this.hero.sidekick,
      alterEgo: this.hero.alterEgo
    });
  }

  get name() { return this.heroForm.get('name'); }
  get power() { return this.heroForm.get('power'); }

  patchValue() {
    this.heroForm.patchValue({
      name: this.hero.name
    });
  }

  rebuildForm() {
    this.heroForm.reset({
      name: this.hero.name // ,
      // address: this.hero.addresses[0] || new Address()
    });
    this.setSecretLairs(this.hero.addresses);
  }

  setSecretLairs(addresses: Address[]) {
    const addressFGs = addresses.map(address => this.fb.group(address));
    const addressFormArray = this.fb.array(addressFGs);
    this.heroForm.setControl('secretLairs', addressFormArray);
  }

  get secretLairs(): FormArray {
    return this.heroForm.get('secretLairs') as FormArray;
  }

  addLair() {
    this.secretLairs.push(this.fb.group(new Address()));
  }

  onSubmit() {
    this.hero = this.prepareSaveHero();
    this.heroService.updateHero(this.hero).subscribe(/* error handling */);
    this.rebuildForm();
  }

  prepareSaveHero(): Hero {
    const formModel = this.heroForm.value;

    // deep copy of form model lairs
    const secretLairsDeepCopy: Address[] = formModel.secretLairs.map(
      (address: Address) => Object.assign({}, address)
    );

    // return new `Hero` object containing a combination of original hero value(s)
    // and deep copies of changed form model values
    const saveHero: Hero = {
      id: this.hero.id,
      name: formModel.name as string,
      // addresses: formModel.secretLairs // <-- bad!
      addresses: secretLairsDeepCopy
    };
    return saveHero;
  }

  revert() { this.rebuildForm(); }
}
