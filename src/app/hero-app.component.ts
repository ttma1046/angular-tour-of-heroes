import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    template: `
    <h1>Tour of Heroes</h1>
    <!--<app-hero-main [hero]="hero"></app-hero-main>-->
  `,
    styles: ['h1 { font-weight: normal; }']
})
export class HeroAppComponent {
    /* . . . */
}
